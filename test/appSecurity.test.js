const request = require('supertest');
const { app, approval } = require('./app');

describe('security', () => {
    it('Request to -500, should throw RangeError exception', async () => {
        expect.assertions(1);
        try {
            approval(-500)
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it('Int32bit maximum amount, should throw RangeError exception', async () => {

        expect.assertions(1);
        try {
            approval(2147483647)
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it('Arithmetic result into Int32bit max value, should throw RangeError exception', async () => {

        expect.assertions(1);
        try {
            approval(2147483647 - 10 + 1)
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });


    it('Arithmetic result into Int32bit max value, should throw RangeError exception', async () => {
        expect.assertions(1);
        try {
            approval(-2147483648 - 10 - 1)
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it('pedram examples', async () => {
        var data = [2147483647, 2147483648, -2147483648, -2147483649, 9223372036854775807, 9223372036854775808, -9223372036854775808,
            -9223372036854775809, -4294967295, -18446744073709551615];
        expect.assertions(data.length);
        for (var i of data ) {
            try {
                approval(i);
            } catch (ex) {
                expect(ex).toBeInstanceOf(RangeError);
            }
        }
    });

    it("should throw RangeError exception for null", async () => {
        expect.assertions(1);
        try {
            approval(null);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("should throw RangeError exception for null", async () => {
        expect.assertions(1);
        try {
            approval(null);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Request in range of [-10.99, ...,  0], should throw RangeError exception ", async () => {
        var data = [-10.99, -10, -9, -1, 0];
        expect.assertions(data.length);
        for (var i of data ) {
            try {
                approval(i);
            } catch (ex) {
                expect(ex).toBeInstanceOf(RangeError);
            }
        }
    });

    it("Request in range of ['-10.99' , ..., '0'], should throw RangeError exception ", async () => {
        var data = ["-10.99", "-10", "-9", "-1", "0"];
        expect.assertions(data.length);
        for (var i of data ) {
            try {
                approval(i);
            } catch (ex) {
                expect(ex).toBeInstanceOf(RangeError);
            }
        }
    });

    it("Request is -11, should throw RangeError exception ", async () => {
        expect.assertions(1);
        try {
            approval(-11);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Request is NaN, should throw RangeError exception ", async () => {
        expect.assertions(1);
        try {
            approval("He is 40");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Request is undefined, should throw RangeError exception ", async () => {
        expect.assertions(1);
        try {
            var x;
            approval(x);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Request is bollean, should throw RangeError exception ", async () => {
        expect.assertions(1);
        try {
            approval(true);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Request is json, should throw RangeError exception ", async () => {
        expect.assertions(1);
        try {
            var x = {name: "Robert", age: 36};
            approval(x);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Arithmetic result into extreme large value, should throw RangeError exception", async () => {
        expect.assertions(1);
        try {
            approval("00000010323245498540985049580495849058043");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Arithmetic result with hex value, should throw RangeError exception", async () => {
        expect.assertions(1);
        try {
            approval("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });

    it("Arithmetic result with Infinity value, should throw RangeError exception", async () => {
        expect.assertions(1);
        try {
            approval(Number.POSITIVE_INFINITY);
        } catch (ex) {
            expect(ex).toBeInstanceOf(RangeError);
        }
    });
});
