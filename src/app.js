'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json({limit: "100kb"}));

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main
app.get('/', (req, res) => {
    try {
        if (approval(req.query.amount)) {
            res.status(400).end(req.query.amount + ' requires approval.');
        } else {
            res.status(200).end(req.query.amount + ' does not require approval.');
        }
    } catch(err) {
        res.status(400).end('bad request');
    }

});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
const approval = (value) => {
    const threshold = 1000;
    const surcharge = 10;
    const parsedAmount = parseInt(value, 10);

    validateParsedAmount(parsedAmount);
    const amount = Int32Array.from([parsedAmount],x => parseInt(x + surcharge))[0];

    if (checkForOverflow(parsedAmount, surcharge, amount)) {
        throw new RangeError("Invalid value");
    }

    return amount > 0 && amount >= threshold;
};

const checkForOverflow = (a, b, c) => {
    return a !== c-b || b !== c-a;
};

const validateParsedAmount = (value) => {
    const maxValue = 1000000000;
    if(typeof value !== "number" || value <= 0 || value > maxValue || isNaN(value)){
        throw RangeError("Invalid value");
    }
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
